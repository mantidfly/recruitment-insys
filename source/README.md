## Notes for the reviewer:

- Project is based on **Create React App** starter kit, to start it run `npm start` inside the **source** folder
- I didn't manage to use the suggested Flickr feed API (https://www.flickr.com/services/feeds/docs/photos_public/), because it doesn't send required CORS headers and it's blocked by the browser. To solve the problem I've used the **Flickr Rest API** which requires API key with every request. In a real project such API should have been connected through server side proxy but I didn't want to complicate the project more than it's necessary. Key is made for temporary fake account so don't worry about privacy.
- I think that I've managed to include all of the bonus elements, though Redux was added mostly to keep and update the state of the gallery. (At first I've completed the project without using Redux library)
