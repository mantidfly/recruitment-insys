import { FlickrApi } from '../apis';
import { updateGallery } from '../actions/gallery';

export const fetchGallery = (tags, size) => {
    return async (dispatch, getState) => {
        try {
            const gallery = await FlickrApi.getPhotos(tags, size);
            dispatch(updateGallery(gallery));
        } catch (ex) {
            console.error(ex);
        }
    }
};