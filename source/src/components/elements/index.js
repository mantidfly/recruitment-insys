import Page from './Page/Page';
import Header from './Header/Header';
import Navbar from './Navbar/Navbar';
import Gallery from './Gallery/Gallery';

export { 
    Page,
    Header,
    Navbar,
    Gallery,
};