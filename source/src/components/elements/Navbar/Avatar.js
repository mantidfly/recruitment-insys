import React from 'react';
import PropTypes from 'prop-types';

import styles from './Avatar.module.scss';

const Avatar = ({ url, className }) => (
    <img src={ url } className={ styles.avatar + ' ' + className } alt="avatar" />
);

Avatar.defaultProps = { 
    className: '',
};

Avatar.propTypes = {
    url: PropTypes.string.isRequired,
};

export default Avatar;