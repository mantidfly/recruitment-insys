import React from 'react';

import styles from './Navbar.module.scss';
import Avatar from './Avatar';
import Navigation from './Navigation';
import MobileNavigation from './MobileNavigation';

import avatarImg from './avatar.jpg';

const menuItems = [
    { 
        url: '/',
        icon: 'user',
        label: 'PROFILE',
    },
    {
        url: '/gallery',
        icon: 'image',
        label: 'GALLERY',
    },
];

const Navbar = () => (
    <div className={ styles.navbar }>
        <Avatar url={ avatarImg } className={ styles.avatar } />
        <Navigation className={ styles.navigation } items={ menuItems } />
        <MobileNavigation items={ menuItems }/>
    </div>
);

export default Navbar;