import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import styles from './Navigation.module.scss';

const Button = ({ url, icon }) => (
    <NavLink 
        to={ url } 
        className={ styles.navLink }
        exact={ true }
        activeClassName={ styles.navLinkActive }
    >
        <FontAwesomeIcon icon={ icon } fixedWidth={ true } size='xs'/>
    </NavLink>
);

const Navigation = ({ className, items }) => (
    <nav className={ styles.navigation + ' ' + className }>
        { items.map((item, i) => (
            <Button 
                key={ i }
                url={ item.url }
                icon={ item.icon } 
            />
        )) }
    </nav>
);

Navigation.defaultProps = { 
    className: '',
    items: [],
};

Navigation.propTypes = {
    className: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
        url: PropTypes.string.isRequired,
        icon: PropTypes.string.isRequired,
    })),
};

export default Navigation;