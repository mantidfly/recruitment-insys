import React, { Component, Fragment } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import styles from './MobileNavigation.module.scss';

const MenuItem = ({ target, label, onClick }) => (
    <NavLink to={ target } className={ styles.navLink } onClick={ onClick }>{ label }</NavLink>
);

class MobileNavigation extends Component {
    state = {
        menuVisible: false,
    };

    toggleMenu = () => {
        this.setState({
            menuVisible: !this.state.menuVisible,
        });
    };

    render = () => {
        const { items } = this.props;
        return (
            <Fragment>
                <button className={ styles.menuButton } onClick={ this.toggleMenu }>
                    <FontAwesomeIcon icon="bars" fixedWidth={ true } size="xs" />
                </button>

                <nav className={ styles.navigation + ' ' + (this.state.menuVisible ? styles.navigationVisible : '') }>
                        { items.map((item, i) => (
                            <MenuItem
                                key={ i }
                                target={ item.url } 
                                label={ item.label }
                                onClick={ this.toggleMenu } 
                            />
                        )) }
                        <button className={ styles.exitButton } onClick={ this.toggleMenu }>
                            <FontAwesomeIcon icon="times" fixedWidth={ true } size="3x" />
                        </button>
                </nav>
            </Fragment>
        );
    };
}

MobileNavigation.defaultProps = {
    items: [],
};

MobileNavigation.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        url: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
    })),
};

export default MobileNavigation;