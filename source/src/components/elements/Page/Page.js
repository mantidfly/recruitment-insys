import React from 'react';

import { Header, Navbar } from '../../elements';

import styles from './Page.module.scss';

const Page = ({ children }) => (
    <div className={ styles.container }>
        <Header />
        <Navbar />
        <div className={ styles.content }>{ children }</div>
    </div>
);

export default Page;