import React from 'react';
import PropTypes from 'prop-types';

import GalleryItem from './GalleryItem';

import styles from './Gallery.module.scss';

const Gallery = ({ items }) => (
    <div className={ styles.gallery }>
        { items.map((item, i) => (
            <GalleryItem 
                key={ i } 
                url={ item.url } 
                thumbnail={ item.thumbnail } 
                alt={ item.alt } 
            />
        )) }
    </div>
);

Gallery.defaultProps = {
    items: [],
};

Gallery.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        url: PropTypes.string.isRequired,
        thumbnail: PropTypes.string.isRequired,
        alt: PropTypes.string.isRequired,
    })),
};

export default Gallery;