import React from 'react';
import PropTypes from 'prop-types';

import styles from './GalleryItem.module.scss';

const GalleryItem = ({ url, thumbnail, alt }) => (
    <picture className={ styles.galleryItem }>
        <a href={ url } target="_blank" rel="noopener noreferrer">
            <img src={ thumbnail } alt={ alt }/>
        </a>
    </picture>
);

GalleryItem.propTypes = {
    url: PropTypes.string.isRequired,
    thumbnail: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
};

export default GalleryItem;