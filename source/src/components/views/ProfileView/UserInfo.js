import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import styles from './UserInfo.module.scss';

const UserInfo = ({ name, city, country }) => (
    <section>
        <h4 className={ styles.name }>{ name }</h4>
        <h5 className={ styles.city }>
            <FontAwesomeIcon className={ styles.icon } icon='map-marker-alt' fixedWidth={ true } size='xs'/>
            { city }, { country }
        </h5>
    </section>
);

export default UserInfo;