import React from 'react';
import UserInfo from './UserInfo';

const ProfileView = () => (
    <article>
        <UserInfo
            name="Marylin Monroe"
            city="Poznan"
            country="PL"
        />
        <p>You wouldn't hit a man no trousers on, would you? Multiply your anger by about a hundred, Kate, that's how much he thinks he love you. My lord! You're a tripod. Boxing is about respect</p>
        <blockquote>
            When a naked man's chasing a woman through an alley <br />
            with a butcher knife and a hard-on, <br />
            I figure he's not out collecting for the Red Cross.
        </blockquote>
        <p>Praesent scelerisque lacus urna, a vehicula elit consectetur nec. Aenean pretium sapien at condimentum accumsan. Nulla mattis rutrum sodales. Quisque scelerisque posuere tempus.</p>
    </article>
);

export default ProfileView;