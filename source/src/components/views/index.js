import ProfileView from './ProfileView/ProfileView';
import GalleryView from './GalleryView/GalleryView';

export { 
    ProfileView,
    GalleryView,
};