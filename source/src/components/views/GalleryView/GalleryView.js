import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchGallery } from '../../../thunks/gallery';
import { Gallery } from '../../elements';

class GalleryView extends Component {
    componentDidMount = async () => {
        this.props.fetchGallery('Marylin Monroe', 9);
    };

    render = () => (
        <Gallery items={ this.props.gallery } />
    );
}

const mapStateToProps = (state) => ({
    gallery: state.gallery.map((photo) => ({
        url: photo.url,
        thumbnail: photo.thumbnail,
        alt: photo.title,
    })),
});

const mapDispatchToProps = (dispatch) => ({
    fetchGallery: (tags, size) => dispatch(fetchGallery(tags, size)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GalleryView);