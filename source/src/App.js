import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faImage, faUser, faMapMarkerAlt, faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { ProfileView, GalleryView } from './components/views';
import { Page } from './components/elements';

import './App.scss';

library.add([faImage, faMapMarkerAlt, faUser, faBars, faTimes]);

const App = () => (
    <Router>
        <Page>
            <Route exact path="/" component={ ProfileView } />
            <Route exact path="/gallery" component={ GalleryView } />
        </Page>
    </Router>
);

export default App;
