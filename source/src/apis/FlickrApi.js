const FLICKR_API_URL = 'https://api.flickr.com/services/rest';

class FlickrApi {
    static getPhotos = async (tags = '', itemsNumber = 10) => {
        const params = {
            method: 'flickr.photos.search',
            tags: tags,
            content_type: 1,
            privacy_filter: 1,
            per_page: itemsNumber,

        };
        return FlickrApi.get(FLICKR_API_URL, params)
            .then((res) => res.photos.photo.map(({ id, farm, server, secret, title }) => ({
                url: createFlickrUrl(id, farm, server, secret, FlickrImageSize.Large),
                thumbnail: createFlickrUrl(id, farm, server, secret, FlickrImageSize.Thumbnail),
                title: title,
            })));
    };

    static get = async (url, params) => {
        return fetch(url + createParams(params), {
                method: 'GET',
                mode: 'cors',
            })
            .then((response) => response.json());
    };
}

const createParams = (params) => paramsToQueryString({
    ...params,
    api_key: process.env.REACT_APP_FLICKR_API_KEY,
    format: 'json',
    nojsoncallback: 1,
});

const paramsToQueryString = (params) => {
    return Object.keys(params).length > 0 
        ? '?' + Object.keys(params).reduce((queryString, key) => { 
                queryString.append(key, params[key]); 
                return queryString; 
            }, new URLSearchParams())
        : '';
};

const FlickrImageSize = {
    Thumbnail: 'm',
    Large: 'h',
    Original: 'o',
};

const createFlickrUrl = (id, farm, server, secret, size) => `https://farm${ farm }.staticflickr.com/${ server }/${ id }_${ secret }_${ size }.jpg`;

export default FlickrApi;