export const UPDATE_GALLERY = 'UPDATE_GALLERY';

export const updateGallery = (gallery) => ({
    type: UPDATE_GALLERY,
    gallery: gallery,
});
