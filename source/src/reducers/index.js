import { combineReducers } from 'redux';

import { profile } from './profile';
import { gallery } from './gallery';

const rootReducer = combineReducers({
    profile: profile,
    gallery: gallery,
});

export default rootReducer;