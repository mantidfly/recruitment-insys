import { UPDATE_GALLERY } from '../actions/gallery';
import _ from 'lodash';

const initialState = [];

export const gallery = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_GALLERY:
            return _.cloneDeep(action.gallery);
        default:
            return state;
    }
};